package com.example.maggie.preparedstatement.gateway;

import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.content.Context;
import android.content.ContentValues;
import android.database.sqlite.SQLiteStatement;

import com.example.maggie.preparedstatement.dbo.Transaction;

import java.util.ArrayList;


/**
 * Created by maggie on 12/04/17.
 */

public class Gateway extends SQLiteOpenHelper {
    private static int DATABASE_VERSION = 1;
    private static String DATABASE_NAME = "MoneyManager.db";
    private static String TABLE_TRANSACTION = "transactions";
    private static String TABLE_TRANSACTION_COLUMN_TRANSACTION_ID = "transaction_id";
    private static String TABLE_TRANSACTION_COLUMN_USER_ID = "user_id";
    private static String TABLE_TRANSACTION_COLUMN_CATEGORY_ID = "category_id";
    private static String TABLE_TRANSACTION_COLUMN_LABEL = "label";
    private static String TABLE_TRANSACTION_COLUMN_AMOUNT = "amount";
    private static String TABLE_TRANSACTION_COLUMN_NECESSARY = "necessary";
    private static String TABLE_TRANSACTION_COLUMN_POSITIVE = "positive";


    public Gateway(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }


    /*
    public Gateway(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION, errorHandler);
    }
    */

    /**
     * Create a new database.
     *
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        StringBuilder query = new StringBuilder("");
        query.append("CREATE TABLE " + TABLE_TRANSACTION + " (");
        query.append(TABLE_TRANSACTION_COLUMN_TRANSACTION_ID + " INT PRIMARY KEY AUTOINCREMENT ");
        query.append(", " + TABLE_TRANSACTION_COLUMN_USER_ID + " INT ");
        query.append(", " + TABLE_TRANSACTION_COLUMN_CATEGORY_ID + " INT ");
        query.append(", " + TABLE_TRANSACTION_COLUMN_LABEL + " TEXT ");
        query.append(", " + TABLE_TRANSACTION_COLUMN_AMOUNT + " REAL ");
        query.append(", " + TABLE_TRANSACTION_COLUMN_NECESSARY + " BOOLEAN ");
        query.append(", " + TABLE_TRANSACTION_COLUMN_POSITIVE + " BOOLEAN ");
        /*
        query.append(", FOREIGN KEY (" + TABLE_TRANSACTION_COLUMN_USER_ID + ") REFERENCES " + );
        query.append(", FOREIGN KEY (" + TABLE_TRANSACTION_COLUMN_CATEGORY_ID + ") REFERENCES ");
        */
        query.append(");");

        db.execSQL(query.toString());
    }


    /**
     * Drop existing tables and create new tables.
     *
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRANSACTION);

        onCreate(db);
    }


    /**
     * Delete a Transaction record from the database.
     *
     * @param id
     */
    public void deleteTransaction(long id) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_TRANSACTION + " WHERE " + TABLE_TRANSACTION_COLUMN_TRANSACTION_ID
                + " = " + id + ";");
        db.close();
    }


    /**
     * Saves a Transaction record to the database.
     * Performs an insert or update operation on the transaction table.
     *
     * @param transaction
     */
    public void saveTransaction(Transaction transaction) {
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = null;
        ArrayList<Object> objectArrayList = new ArrayList<>();
        String simpleQueryResult = null;

        if (transaction.getTransaction_id() != null && !transaction.getTransaction_id().equals(0L)) {
            String sql = "SELECT " + TABLE_TRANSACTION_COLUMN_TRANSACTION_ID + " FROM " + TABLE_TRANSACTION
                    + " WHERE " + TABLE_TRANSACTION_COLUMN_TRANSACTION_ID + " = ?;";

            db.beginTransaction();
            SQLiteStatement sqLiteStatement = db.compileStatement(sql);
            sqLiteStatement.bindLong(1, transaction.getTransaction_id());

            // Execute the SQL transaction.
            simpleQueryResult = sqLiteStatement.simpleQueryForString();
            db.setTransactionSuccessful();
            db.endTransaction();
        }

        // If the record already exists, perform an update transaction.
        if (transaction.getTransaction_id() != null
                && simpleQueryResult != null) {

            db.beginTransactionNonExclusive();
            StringBuilder updateQuery = new StringBuilder("UPDATE " + TABLE_TRANSACTION + " ");

            // Set the transaction_id.
            if (transaction.getTransaction_id() != null) {
                if (objectArrayList.size() == 0) {
                    updateQuery.append(" SET ");
                } else {
                    updateQuery.append(" , ");
                }
                updateQuery.append(TABLE_TRANSACTION_COLUMN_TRANSACTION_ID + " = ? ");
                objectArrayList.add(transaction.getTransaction_id());
            }

            // Set the user_id
            if (transaction.getUser_id() != null) {
                if (objectArrayList.size() == 0) {
                    updateQuery.append(" SET ");
                } else {
                    updateQuery.append(" , ");
                }
                updateQuery.append(TABLE_TRANSACTION_COLUMN_USER_ID + " = ? ");
                objectArrayList.add(transaction.getUser_id());
            }

            // Set the category_id
            if (transaction.getCategory_id() != null) {
                if (objectArrayList.size() == 0) {
                    updateQuery.append(" SET ");
                } else {
                    updateQuery.append(" , ");
                }
                updateQuery.append(TABLE_TRANSACTION_COLUMN_CATEGORY_ID + " = ? ");
                objectArrayList.add(transaction.getCategory_id());
            }

            // Set the label
            if (transaction.getLabel() != null) {
                if (objectArrayList.size() == 0) {
                    updateQuery.append(" SET ");
                } else {
                    updateQuery.append(" , ");
                }
                updateQuery.append(TABLE_TRANSACTION_COLUMN_LABEL + " = ? ");
                objectArrayList.add(transaction.getLabel());
            }

            // Set the amount
            if (transaction.getAmount() != null) {
                if (objectArrayList.size() == 0) {
                    updateQuery.append(" SET ");
                } else {
                    updateQuery.append(" , ");
                }
                updateQuery.append(TABLE_TRANSACTION_COLUMN_AMOUNT + " = ? ");
                objectArrayList.add(transaction.getAmount());
            }

            // Set the necessary
            if (transaction.getNecessary() != null) {
                if (objectArrayList.size() == 0) {
                    updateQuery.append(" SET ");
                } else {
                    updateQuery.append(" , ");
                }
                updateQuery.append(TABLE_TRANSACTION_COLUMN_NECESSARY + " = ? ");
                objectArrayList.add(transaction.getNecessary());
            }

            // Set the positive
            if (transaction.getPositive() != null) {
                if (objectArrayList.size() == 0) {
                    updateQuery.append(" SET ");
                } else {
                    updateQuery.append(" , ");
                }
                updateQuery.append(TABLE_TRANSACTION_COLUMN_POSITIVE + " = ? ");
                objectArrayList.add(transaction.getPositive());
            }

            // Specify the record to update based on the transaction id.
            updateQuery.append(" WHERE " + TABLE_TRANSACTION_COLUMN_TRANSACTION_ID + " = ? ;");
            objectArrayList.add(transaction.getTransaction_id());

            // Get the prepared statement.
            SQLiteStatement updateStatement = db.compileStatement(updateQuery.toString());

            // Set the individual objects in the prepared statement.
            for (int i = 0; i < objectArrayList.size(); i++) {
                Object object = objectArrayList.get(i);

                if (object instanceof Long) {
                    updateStatement.bindLong(i + 1, ((Long) object));
                } else if (object instanceof Double) {
                    updateStatement.bindDouble(i + 1, ((Double) object));
                } else {
                    updateStatement.bindString(i + 1, ((String) object));
                }
            }

            // Execute  the update statement.
            updateStatement.executeUpdateDelete();
            db.setTransactionSuccessful();
            db.endTransaction();
        }
        // The record does not exist. Perform an insert transaction.
        else {
            ContentValues contentValues = new ContentValues();
            contentValues.put(TABLE_TRANSACTION_COLUMN_USER_ID, transaction.getUser_id());
            contentValues.put(TABLE_TRANSACTION_COLUMN_CATEGORY_ID, transaction.getCategory_id());
            contentValues.put(TABLE_TRANSACTION_COLUMN_LABEL, transaction.getLabel());
            contentValues.put(TABLE_TRANSACTION_COLUMN_AMOUNT, transaction.getAmount());
            contentValues.put(TABLE_TRANSACTION_COLUMN_NECESSARY, transaction.getNecessary());
            contentValues.put(TABLE_TRANSACTION_COLUMN_POSITIVE, transaction.getPositive());

            db.beginTransactionNonExclusive();
            db.insert(TABLE_TRANSACTION, null, contentValues);
            db.setTransactionSuccessful();
            db.endTransaction();
        }

        // Close the database connection.
        db.close();
    }

    /**
     * Gets a Transaction record from the database with the given transaction_id.
     *
     * @param id
     * @return Transaction
     */
    public Transaction getTransactionByTransactionId(Long id) {
        SQLiteDatabase db = getReadableDatabase();
        Transaction transaction = new Transaction();
        String query = "SELECT * FROM " + TABLE_TRANSACTION
                + " WHERE " + TABLE_TRANSACTION_COLUMN_TRANSACTION_ID + " = '" + id + "';";

        // Execute the query, and get the results.
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        // Get the SQL transaction results. In theory, the SQL query should only return one result.
        // This loop is created just in case there are no transaction records with the given id.
        while (!cursor.isAfterLast()) {
            if (cursor.getLong(cursor.getColumnIndex(TABLE_TRANSACTION_COLUMN_TRANSACTION_ID)) != 0) {
                transaction.setTransaction_id(cursor.getLong(
                        cursor.getColumnIndex(TABLE_TRANSACTION_COLUMN_TRANSACTION_ID)));
                transaction.setUser_id(cursor.getLong(
                        cursor.getColumnIndex(TABLE_TRANSACTION_COLUMN_USER_ID)));
                transaction.setCategory_id(cursor.getLong(
                        cursor.getColumnIndex(TABLE_TRANSACTION_COLUMN_CATEGORY_ID)));
                transaction.setLabel(cursor.getString(
                        cursor.getColumnIndex(TABLE_TRANSACTION_COLUMN_LABEL)));
                transaction.setAmount(cursor.getDouble(
                        cursor.getColumnIndex(TABLE_TRANSACTION_COLUMN_AMOUNT)));
                /*
                transaction.setNecessary(BooleanUtils.toBoolean(
                        cursor.getInt(cursor.getColumnIndex(TABLE_TRANSACTION_COLUMN_NECESSARY))));
                transaction.setPositive(BooleanUtils.toBoolean(
                        cursor.getInt(cursor.getColumnIndex(TABLE_TRANSACTION_COLUMN_POSITIVE))));
                 */
            } // end if (cursor.getLong(cursor.getColumnIndex(TABLE_TRANSACTION_COLUMN_ID)) != 0)

            // Move the cursor to the next result to prevent infinite looping.
            cursor.moveToNext();
        }

        // Close the database connection.
        db.close();

        // Return the search result.
        return transaction;
    }

}


